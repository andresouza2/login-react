import axios from "axios";

const api = axios.create({
  baseURL: import.meta.env.VITE_APP_API,
});

export const useApi = () => ({
  validateToken: async (token: string) => {
    return {
      user: {
        id: 1,
        name: "André",
        email: "andre@andre.com",
      },
    };
    /* const response = await api.post("/token", { token });
    return response.data; */
  },

  signin: async (email: string, password: string) => {
    return {
      user: { id: 1, name: "André", email: "andre@andre.com" },
      token: "12334567890",
    };
    /* const response = await api.post("/signin", { email, password });
    return response.data; */
  },

  signout: async () => {
    return { status: true };
    /* const response = await api.post("/logout");
    return response.data; */
  },
});
