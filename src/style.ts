import styled from "styled-components"

export const Style = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  text-align: center;

  header {
    margin-top: 1em;
    margin-bottom: 2em;

    nav {
      margin-bottom: 0.5em;
      a {
        margin: 5px 20px;
      }

    }

    
  }
`