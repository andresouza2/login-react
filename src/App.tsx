import { useContext } from "react";
import { Route, Routes, Link } from "react-router-dom";
import { AuthContext } from "./contexts/Auth/AuthContext";
import RequireAuth from "./contexts/Auth/RequireAuth";
import Private from "./pages/Private";
import Home from "./pages/Home";
import { Style } from "./style";

function App() {
  const auth = useContext(AuthContext);

  const handleLogout = async () => {
    await auth.signout();
    window.location.href = window.location.href;
  };

  return (
    <div className="App">
      <Style>
        <header>
          <h1>2D innovation</h1>
          <nav>
            <Link to="/">Home</Link>
            <Link to="/private">Página Privada</Link>
            {auth.user && (
              <a href="#" onClick={handleLogout}>
                sair
              </a>
            )}
          </nav>
          <hr />
        </header>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="/private"
            element={
              <RequireAuth>
                <Private />
              </RequireAuth>
            }
          />
        </Routes>
      </Style>
    </div>
  );
}

export default App;
